<?php
namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;

class AuthController extends Controller{
    
    public function signupAndAuthenticate(Request $request){
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required'
        ]);
        
        //first add user to db
        $user = new User;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        if($user->save()){
            //then authenticate user 
            $http = new Client([
                'timeout'=> 15
            ]);
                
            try{
                $request = $http->post(url('/oauth/token'), [
                    'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => 2,
                        'client_secret' => 'vUpjmLth1SFibz9QqoxCK6CpPVasiKyaTsvl5ysz',
                        'username' => $request->email,
                        'password' => $request->password,
                        'scope' => '',
                    ],
                ]);

                $response = $request->getBody();
                $response = json_decode($response);

            }catch(ClientException $e){
                $error = $e->getMessage();
                $errorCode = $e->getCode();

            }catch(ConnectException $e){
                $error = $e->getMessage();
                $errorCode = $e->getCode();

            }catch(RequestException $e){
                $error = $e->getMessage();
                $errorCode = $e->getCode();
            }

            if(!isset($error)){
                return repsonse()->json([
                    'message' => 'oauth Token generated successfully',
                    'data' => $response
                ],$request->getStatusCode());

            }else{
                return response()->json([
                    'message' => $error
                ], $errorCode);

            }


        }

    }

    public function getOauthToken(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        
        //Check if user exists in db
        $user = User::where('email',$request->email)->first();

        if(!$user){
            return response()->json([
                'message' => 'user not found'
            ],404);

        }else{
            //Check if password matches
            if(Hash::check($request->password,$user->password)){
                $http = new Client;
                $requestBody = [
                    'grant_type' => 'password',
                    'client_id' => 2,
                    'client_secret' => 'vUpjmLth1SFibz9QqoxCK6CpPVasiKyaTsvl5ysz',
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => '',
                ];
                $requestBody = json_encode($requestBody);
                try{
                    $request = $http->post(url('/oauth/token'),[
                        'body' => $requestBody
                    ]);
    
                    $response = $request->getBody();
                    $response = json_decode($response);
    
                }catch(ClientException $e){
                    $error = $e->getMessage();
                    $errorCode = $e->getCode();
    
                }catch(ConnectException $e){
                    $error = $e->getMessage();
                    $errorCode = $e->getCode();
                }
    
                if(!isset($error)){
                    return repsonse()->json([
                        'message' => 'oauth Token generated successfully',
                        'data' => $response
                    ],$request->getStatusCode());
    
                }else{
                    return response()->json([
                        'message' => $error
                    ], $errorCode);
    
                }
    
            }else{
                return response()->json([
                    'invalid crefentials chief!',
                    'data' => $request
                ],404);
            }
        }


        
    }


}